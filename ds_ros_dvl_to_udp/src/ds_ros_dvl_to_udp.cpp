#include "ros/ros.h"
#include "ds_sensor_msgs/Dvl.h"

#include "ds_ros_dvl_to_udp/ds_ros_dvl_to_udp.h"

namespace ds_ros_dvl_to_udp
{
  DsRosDvlToUdp::~DsRosDvlToUdp() = default;

  DsRosDvlToUdp::DsRosDvlToUdp() : DsProcess()
  {
  }

  DsRosDvlToUdp::DsRosDvlToUdp(int argc, char* argv[], const std::string& name) : DsProcess(argc, argv, name)
  {
  }


  void DsRosDvlToUdp::setupConnections()
  {
    DsProcess::setupConnections();

    m_conn = addConnection("out_connection", boost::bind(&DsRosDvlToUdp::on_in_msg, this, _1));

  }
  

  void DsRosDvlToUdp::setupSubscriptions()
  {
    DsProcess::setupSubscriptions();

    auto nh = nodeHandle();

    m_dvl_sub= nh.subscribe<ds_sensor_msgs::Dvl>(m_dvl_topic, 1, boost::bind(&DsRosDvlToUdp::onDvlMsg, this, _1));

  }

  void DsRosDvlToUdp::setupParameters()
  {
    DsProcess::setupParameters();

    m_dvl_topic = ros::param::param<std::string>("~dvl_topic", "0");
  }

  void DsRosDvlToUdp::onDvlMsg(const ds_sensor_msgs::Dvl::ConstPtr& msg)
  {
    auto os = std::ostringstream{};
    double timestamp = msg->header.stamp.sec + (double) msg->header.stamp.nsec/1e9;

    double alt;
    if (std::isnan(msg->altitude))
      {
	alt = -1;
      }
    else
      {
	alt = msg->altitude;
      }
    
    os << "DSDVL" << ","
       << std::fixed << std::setprecision(6)
       << timestamp << ","
       << msg->velocity.x << ","
       << msg->velocity.y << ","
       << msg->velocity.z << ","
       << msg->range[0] << ","
       << msg->range[1] << ","
       << msg->range[2] << ","
       << msg->range[3] << ","
       << std::fixed << std::setprecision(3)
       << alt << ","
       << msg->speed_sound << ","
       << static_cast<int>(msg->num_good_beams) << ","
      << static_cast<int>(msg->dvl_type) << std::endl;

    m_conn->send(os.str());

  }

  void DsRosDvlToUdp::on_in_msg(const ds_core_msgs::RawData& bytes)
  {

  }


}

