#ifndef DS_ROS_DVL_TO_UDP_H
#define DS_ROS_DVL_TO_UDP_H

#include "ds_base/ds_process.h"
#include "ds_sensor_msgs/Dvl.h"

namespace ds_ros_dvl_to_udp
{
class DsRosDvlToUdp : public ds_base::DsProcess
{
public:
  DsRosDvlToUdp();

  DsRosDvlToUdp(int argc, char* argv[], const std::string& name);
  ~DsRosDvlToUdp() override;

protected:
  void setupConnections() override;
  void setupSubscriptions() override;
  void setupParameters() override;

  void onDvlMsg(const ds_sensor_msgs::Dvl::ConstPtr& msg);

  void on_in_msg(const ds_core_msgs::RawData& bytes);

private:
  // ********** CONNECTIONS
  // Outputs DVl converted data from ros Dvl on udp
  boost::shared_ptr<ds_asio::DsConnection> m_conn;

  ros::Subscriber m_dvl_sub;
  std::string m_dvl_topic;
  ros::NodeHandle nh;

  
};
}

#endif
